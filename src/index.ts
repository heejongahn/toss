import { getFirstElemByClass, formatAmount } from './utils'
import { fetchAccounts, fetchUserInfo } from './api'
import { Account, User } from "./types";
import * as Flickity from "flickity";

import './style.scss'
import '../node_modules/flickity/dist/flickity.min.css'

// Initialization
const userId = '817c2a9'
let user: User
let accountsCarousel: Flickity

const accountsContainer = getFirstElemByClass('accounts')

const bankNameMap: { [id: string]: string } = {
    toss: "Toss",
    Shinhan: '신한',
    Kookmin: '국민',
    /* ... */
}

async function main() {
    const loadingSplash = getFirstElemByClass('loading-splash')
    const accounts = await fetchAccounts(userId)

    user = {
        ...await fetchUserInfo(userId),
        accounts: accounts.sort(a => {
            if (a.corporation.id === 'toss') {
                return -1
            }
            return 1
        })
    }

    // Populate accounts
    accounts.forEach(account => {
        const { account: accountId, corporation, deposit } = account
        const elem = document.createElement('div')
        elem.className = 'account-item'
        elem.dataset.account = accountId

        const bankName = bankNameMap[corporation.id]

        const accountName = document.createElement('div')
        accountName.innerText = bankName === 'Toss'
            ? 'Toss 주계좌'
            : `${bankName} 계좌`
        accountName.className = 'account-name'

        const additionalInfo = document.createElement('div')
        additionalInfo.innerText = deposit.amount
            ? `${deposit.amount.toLocaleString()} 원`
            : `${bankName} 계좌 ${accountId}`
        additionalInfo.className = 'account-additional-info'

        const dimmer = document.createElement('div')
        dimmer.className = 'account-dimmer'

        elem.appendChild(accountName)
        elem.appendChild(additionalInfo)
        elem.appendChild(dimmer)

        accountsContainer.appendChild(elem)
    })

    accountsCarousel = new Flickity(accountsContainer, {
        cellSelector: '.account-item',
        draggable: true,
        pageDots: true,
        prevNextButtons: false,
    })

    accountsCarousel.on('select', function () {
        const selectedAccount = user.accounts[accountsCarousel.selectedIndex]
        console.log(selectedAccount)
        updateSend()
    })

    amountLabel.innerText = `보낼 금액 (최대 ${user.limit.remain.toLocaleString()}원)`

    loadingSplash.style.opacity = '0'
    loadingSplash.style.visibility = 'hidden'
}

const amountInput = getFirstElemByClass('amount-input') as HTMLInputElement

function getCurrentAmount() {
    const amountString = amountInput.value
    return amountString === ''
        ? null
        : parseInt(amountString)
}

function getSelectedAccount() {
    const selectedAccount = user.accounts[accountsCarousel.selectedIndex]
    if (!selectedAccount) {
        throw new Error('Invalid account is selected.')
    }

    return selectedAccount
}

// Amount
const amountLabel = getFirstElemByClass('amount-label')
const amountPopup = getFirstElemByClass('amount-popup')
const amountInputForm = getFirstElemByClass('amount-input-form')
const amountInputOpener = getFirstElemByClass('amount-input-opener')

amountInputOpener.addEventListener('click', () => {
    amountPopup.style.visibility = 'visible'
    amountPopup.style.opacity = '1'
    amountInput.focus()
})

const closeAmountPopup = () => {
    amountPopup.style.opacity = '0'
    amountPopup.style.visibility = 'hidden'
}

amountInputForm.addEventListener('submit', closeAmountPopup)
amountInput.addEventListener('blur', closeAmountPopup)

const confirmButton = getFirstElemByClass('confirm-button')
confirmButton.addEventListener('click', closeAmountPopup)

amountInput.addEventListener('input', e => {
    const previousAmount = amountInputOpener.innerText || '0'

    const target = e.target as HTMLInputElement
    target.value = `${updateAmountHelper(target.value)}`

    let newAmount = parseInt(target.value).toLocaleString()

    if (previousAmount === newAmount) {
        // Nothing has changed
        return
    }

    if (target.value === '') {
        target.value = '0';
    }

    newAmount = parseInt(target.value).toLocaleString()
    updateAmountWindow(previousAmount, newAmount)

    updateAccount()
    updateSend()
})

function updateAmountWindow(previousAmount: string, newAmount: string) {
    const amountInputWindow = getFirstElemByClass('amount-input-window')
    const amountInputLastCharacter = getFirstElemByClass('amount-input-last-character')

    // Hide input opener text when newAmount === '0'
    amountInputOpener.innerHTML = newAmount === '0' ? '' : newAmount

    amountInputWindow.innerHTML = ''
    amountInputLastCharacter.innerHTML = ''

    amountInputWindow.innerHTML = newAmount.substr(0, newAmount.length - 1)
    if (previousAmount.length < newAmount.length) {
        amountInputLastCharacter.classList.remove('fall')
        void amountInputLastCharacter.offsetWidth // Trigger reflow
        amountInputLastCharacter.classList.add('fall')
    }

    amountInputLastCharacter.innerText = newAmount[newAmount.length - 1]

}

function updateAmountHelper(amountString: string) {
    // TODO: Rename ?
    const amountHelper = getFirstElemByClass('amount-helper')
    const { daily: dailyLimit, remain: remainingLimit } = user.limit

    const amount = parseInt(amountString)
    if (Number.isNaN(amount)) {
        amountHelper.innerText = ''
        return ''
    }

    if (amount < remainingLimit) {
        amountHelper.classList.remove('error')
        amountHelper.innerText = formatAmount(amount)

        return amount
    } else {
        amountHelper.classList.add('error')
        if (dailyLimit < amount) {
            amountHelper.innerText = `1일 최대 ${dailyLimit.toLocaleString()}원 까지만 이체할 수 있습니다.`
            return dailyLimit
        } else {
            /*
            NOTE:
            토스 앱은 항상 1일 최대 메시지를 보여주는 것 같은데, 실제 현시점 한도를 보여주는게 더 친절할 것 같아서 이렇게 했습니다.
            */
            amountHelper.innerText = `오늘 최대 ${remainingLimit.toLocaleString()}원 까지만 더 이체할 수 있습니다.`
            return remainingLimit
        }
    }
}

// Account
const accountSelection = getFirstElemByClass('account-selection')

function updateAccount() {
    const amount = getCurrentAmount()
    if (!amount) {
        accountSelection.style.visibility = 'hidden'
        return
    }

    accountSelection.style.visibility = 'initial'
}

// Send
const send = getFirstElemByClass('send')
function updateSend() {
    const amount = getCurrentAmount()
    const account = getSelectedAccount()

    if (!amount) {
        send.style.visibility = 'hidden';
        return
    }

    send.style.visibility = 'initial'

    updateSendHelperBadge(amount, account)
    updateSendButton(amount, account)
}

function updateSendHelperBadge(amount: number, account: Account) {
    const tossSufficientBadge = getFirstElemByClass('toss-sufficient')
    const tossInsufficientBadge = getFirstElemByClass('toss-insufficient')
    const wireFreeRemainingBadge = getFirstElemByClass('wire-free-remaining')

    tossSufficientBadge.style.display = 'none';
    tossInsufficientBadge.style.display = 'none';
    wireFreeRemainingBadge.style.display = 'none';

    const { corporation, deposit } = account

    if (corporation.id === 'toss') {
        if (deposit.amount === null) {
            throw new Error('Toss account\'s deposit is null.')
        }

        if (deposit.amount > amount) {
            tossSufficientBadge.style.display = 'block'
        } else {
            tossInsufficientBadge.style.display = 'block'
        }
    } else {
        /*
        NOTE:
        주어진 API 스펙에는 Account 정보에 무료 송금이 몇 회 남았는지가 빠져 있어서 항상 5회로 두었습니다.
        만약 API가 해당 정보를 준다면 innerHTML도 같이 갈아줘야 할 것입니다.
        */
        wireFreeRemainingBadge.style.display = 'block';
    }
}

const sendButton = getFirstElemByClass('send-button') as HTMLButtonElement

function updateSendButton(amount: number, account: Account) {
    const { corporation, deposit } = account
    if (corporation.id === 'toss') {
        if (deposit.amount === null) {
            throw new Error('Toss account\'s deposit is null.')
        }

        if (deposit.amount > amount) {
            sendButton.disabled = false
        } else {
            sendButton.disabled = true
        }
    } else {
        sendButton.disabled = false
    }

}

sendButton.addEventListener('click', () => {
    const amount = getCurrentAmount()
    const selectedAccount = getSelectedAccount()

    /*
    NOTE:
    이후에는 Currency 관련 정보도 들어가야 할 것 같습니다.
    */
    console.log(`
    API Call payload:

    {
        userId: ${userId}
        isTossAccount: ${selectedAccount.corporation.id === 'toss'}
        account: ${selectedAccount.account}
        amount: ${amount}
        receiverId: ${123456} (미구현)
    }
    `)
})

main()
