export async function fetchUserInfo(userId: string) {
    console.log(`Requesting 'fetchUserInfo' for user ${userId}.`)
    /*
    NOTE:
    임의로 3초를 로딩 시간으로 설정했습니다.
    */
    return new Promise(resolve => setTimeout(resolve, 3000)).then(() => {
        return {
            "id": "817c2a9",
            "name": "",
            "limit": {
                "daily": 2000000,
                "remain": 2000000
            }
        }
    })
}

export async function fetchAccounts(userId: string) {
    console.log(`Requesting 'fetchAccounts' for user ${userId}.`)
    return Promise.resolve([
        {
            "corporation": {
                "id": "Shinhan",
                "name": " "
            },
            "account": "4648*****84347",
            "deposit": {
                "amount": null,
                "currency": ""
            },
            "fee": 500
        },
        /*
        NOTE:
        계좌 carousel의 모든 UI 경우를 다 확인할 수 있도록 계좌를 하나 추가했습니다.
        */
        {
            "corporation": {
                "id": "Kookmin",
                "name": " "
            },
            "account": "1234*****56789",
            "deposit": {
                "amount": null,
                "currency": ""
            },
            "fee": 500
        },
        {
            "corporation": {
                "id": "toss",
                "name": "Toss "
            },
            "account": "",
            "deposit": {
                "amount": 141000,
                "currency": ""
            },
            "fee": 0
        }
    ])
}
