export function getFirstElemByClass(className: string): HTMLElement {
    const elements = document.getElementsByClassName(className)
    if (elements.length === 0) {
        throw new Error('Thers\'s no such element.')
    }

    return elements[0] as HTMLElement
}

export function formatAmount(amount: number) {
    const [first, second] = [
        Math.floor(amount / 10000),
        amount % 10000
    ]

    if (first === 0) {
        return ''
    }

    if (second === 0) {
        if (first === 1) {
            return ''
        }

        return `${first.toLocaleString()}만 원`
    }

    return `${first.toLocaleString()}만 ${second.toLocaleString()} 원`
}
