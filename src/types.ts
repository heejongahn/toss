type UUID = string

export interface User {
    id: UUID;
    name: string;
    limit: {
        daily: number;
        remain: number;
    }
    accounts: Array<Account>
}

export interface Account {
    account: string;
    corporation: {
        id: string;
        name: string;
    };
    deposit: {
        amount: number | null;
        currency: string
    },
    fee: number;
}
