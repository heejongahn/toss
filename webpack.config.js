const { resolve } = require('path')
const { CheckerPlugin } = require('awesome-typescript-loader')
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin")

module.exports = {
  // Currently we need to add '.ts' to the resolve.extensions array.
  entry: resolve('./src/index.ts'),
  output: {
    path: resolve('./public'),
    filename: 'index.js'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  // Source maps support ('inline-source-map' also works)
  devtool: 'source-map',
  // Add the loader for .ts files.
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.css?$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ]
      },
      {
        test: /\.scss?$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      }
    ],
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: resolve('./static'), to: resolve('./public') }
    ]),
    new CheckerPlugin(),
    new ExtractTextPlugin('style.css')
  ]
};
